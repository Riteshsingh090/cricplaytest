package com.cricplaytest.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.cricplaytest.R;
import com.cricplaytest.adapter.MatchAdapter;
import com.cricplaytest.adapter.VerticalSpaceItemDecoration;
import com.cricplaytest.constant.Constant;
import com.cricplaytest.data.DataManager;
import com.cricplaytest.models.ContestList;
import com.cricplaytest.models.MyContest;
import com.cricplaytest.network.RetrofitUtils;
import com.cricplaytest.network.URLConstant;
import com.cricplaytest.presenter.ContestContract;
import com.cricplaytest.presenter.ContestPresenter;
import com.cricplaytest.utils.AppUtils;
import com.cricplaytest.utils.DateUtil;
import com.cricplaytest.utils.PaginationScrollListener;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ritesh5.singh on 28/10/18.
 */

public class MainActivity extends Activity implements MatchAdapter.Callback, ContestContract.View{

  private MatchAdapter myContestAdapter;

  private ProgressBar pbLoader;
  private Button participate_now;
  private RecyclerView recyclerView;

  private ContestPresenter preseneter;
  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    recyclerView= findViewById(R.id.rvMyContest);
    pbLoader = findViewById(R.id.pbLoader);
    participate_now = findViewById(R.id.participate_now);
    initAdapter(recyclerView);
    preseneter= new ContestPresenter();
    preseneter.onCreate(this);
    preseneter.loadContestList(this);
  }



  private void initAdapter(RecyclerView recyclerView) {
    LinearLayoutManager linearLayoutManager =
        new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    myContestAdapter = new MatchAdapter(this, this);
    recyclerView.setLayoutManager(linearLayoutManager);
    recyclerView.addItemDecoration(new VerticalSpaceItemDecoration((int)getResources().getDimension(R.dimen.space_10dp)));
    recyclerView.setAdapter(myContestAdapter);

    recyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
      @Override
      protected void loadMoreItems() {
        preseneter.loadContestList(MainActivity.this);
        }

      @Override
      public int getTotalPageCount() {
        return Constant.TOTAL_PAGES;
      }

      @Override
      public boolean isLastPage() {
        return preseneter.isLastPage();
      }

      @Override
      public boolean isLoading() {
        return preseneter.isLoading();
      }
    });
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

  }


  @Override
  public void showLoading() {
    pbLoader.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoading() {
    pbLoader.setVisibility(View.GONE);
  }

  @Override
  public void showMessage(int message) {
    Toast.makeText(this,getString(message),Toast.LENGTH_LONG).show();
  }

  @Override
  public void goTOContest(ContestList contest) {

  }

  @Override
  public void addContestList(ArrayList<Object> response) {
    myContestAdapter.addItem(response);
  }

  @Override
  public void showEmptyView() {
    participate_now.setVisibility(View.VISIBLE);
    recyclerView.setVisibility(View.GONE);
  }

  @Override
  public void onSelectGoTOContest(String phoneNum, String title, String message) {

  }
}
