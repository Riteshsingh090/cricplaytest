package com.cricplaytest.application;

import android.app.Application;
import com.squareup.leakcanary.LeakCanary;

/**
 * Created by ritesh singh on 23/10/18.
 */

public class CricPlayTestApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);
    }
}
