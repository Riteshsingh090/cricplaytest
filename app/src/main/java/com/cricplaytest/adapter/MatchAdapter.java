package com.cricplaytest.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.cricplaytest.R;
import com.cricplaytest.constant.Constant;
import com.cricplaytest.models.MyContest;
import com.cricplaytest.models.Team1;
import com.cricplaytest.models.Team2;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ritesh singh on 28/10/18.
 */

public class MatchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Callback callback;
  private Context mcontext;
  private ArrayList<Object> cotestList;
  private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();

  public interface Callback {
    void onSelectGoTOContest(String phoneNum, String title, String message);
  }

  public MatchAdapter(Context context, Callback callback) {
    this.mcontext = context;
    this.callback = callback;
    cotestList = new ArrayList<>();
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    RecyclerView.ViewHolder viewHolder = null;
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    switch (viewType) {
      case 1:
        viewHolder = new ViewHolderDate(inflater.inflate(R.layout.date_row, parent, false));
        break;
      case 2:
        viewHolder = new ViewHolderContest(inflater.inflate(R.layout.mycontest_row, parent, false));
        break;
    }

    return viewHolder;
  }

  static class ViewHolderDate extends RecyclerView.ViewHolder {
    TextView dateTV;

    public ViewHolderDate(View itemView) {
      super(itemView);
      dateTV = itemView.findViewById(R.id.dateTV);
    }
  }

   class ViewHolderContest extends RecyclerView.ViewHolder {
    ImageView team1Icon;
    ImageView imageView_upper;
    TextView textViewTeam1;
    TextView tvScoreTeam1;
    TextView tvOverTeam1;
    ImageView team2Icon;
    ImageView imageView_upperteam2;
    TextView tvTeam2Name;
    TextView tvScoreTeam2;
    TextView tvOverTeam2;
    TextView matchStatus;
    RecyclerView rvContest;

    public ViewHolderContest(View itemView) {
      super(itemView);
      team1Icon = itemView.findViewById(R.id.team1Icon);
      imageView_upper = itemView.findViewById(R.id.imageView_upper);
      textViewTeam1 = itemView.findViewById(R.id.textViewTeam1);
      tvScoreTeam1 = itemView.findViewById(R.id.tvScoreTeam1);
      tvOverTeam1 = itemView.findViewById(R.id.tvOverTeam1);
      team2Icon = itemView.findViewById(R.id.team2Icon);
      imageView_upperteam2 = itemView.findViewById(R.id.imageView_upperteam2);
      tvTeam2Name = itemView.findViewById(R.id.tvTeam2Name);
      tvScoreTeam2 = itemView.findViewById(R.id.tvScoreTeam2);
      tvOverTeam2 = itemView.findViewById(R.id.tvOverTeam2);
      matchStatus = itemView.findViewById(R.id.matchStatus);
      rvContest = itemView.findViewById(R.id.rvContest);
      LinearLayoutManager linearLayoutManager =
          new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false);
      rvContest.addItemDecoration(new VerticalSpaceItemDecoration((int)mcontext.getResources().getDimension(R.dimen.space_10dp)));
      rvContest.setLayoutManager(linearLayoutManager);
      rvContest.setRecycledViewPool(viewPool);
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    switch (holder.getItemViewType()) {
      case 1:
        ViewHolderDate viewHolderDate = (ViewHolderDate) holder;
        bindViewDate(viewHolderDate, position);
        break;
      case 2:
        ViewHolderContest viewHolderContest = (ViewHolderContest) holder;
        bindViewContest(viewHolderContest, position);
        break;
    }
  }

  private void bindViewDate(ViewHolderDate hodlerDate, int pPosition) {
    hodlerDate.dateTV.setText((String) cotestList.get(pPosition));
  }

  private void bindViewContest(ViewHolderContest hodlerMatch, int pPosition) {

    MyContest contest = (MyContest) cotestList.get(pPosition);
    Team1 team1 = contest.getTeam1();
    Team2 team2 = contest.getTeam2();
    if (!TextUtils.isEmpty(team1.getImage())) {
      Picasso.get()
          .load(team1.getImage())
          .placeholder(R.drawable.mumbai)
          .error(R.drawable.mumbai)
          .into(hodlerMatch.team1Icon);
    } else {
      Picasso.get().load(R.drawable.mumbai).into(hodlerMatch.team1Icon);
    }
    hodlerMatch.textViewTeam1.setText(team1.getAlias());
    if (Constant.TOBAT.equals(team1.getState())) {
      hodlerMatch.tvScoreTeam1.setText(mcontext.getString(R.string.did_not_bat));
      hodlerMatch.tvOverTeam1.setText("");
    } else {
      hodlerMatch.tvScoreTeam1.setText(team1.getScore());
      hodlerMatch.tvOverTeam1.setText(String.valueOf(team1.getOvers()));
    }
    if (team1.getWinner()) {
      hodlerMatch.imageView_upper.setVisibility(View.VISIBLE);
    } else {
      hodlerMatch.imageView_upper.setVisibility(View.GONE);
    }

    if (!TextUtils.isEmpty(team2.getImage())) {
      Picasso.get()
          .load(team2.getImage())
          .placeholder(R.drawable.mumbai)
          .error(R.drawable.mumbai)
          .into(hodlerMatch.team2Icon);
    } else {
      Picasso.get().load(R.drawable.mumbai).into(hodlerMatch.team2Icon);
    }
    hodlerMatch.tvTeam2Name.setText(team2.getAlias());
    if (Constant.TOBAT.equals(team2.getState())) {
      hodlerMatch.tvScoreTeam2.setText(mcontext.getString(R.string.did_not_bat));
      hodlerMatch.tvOverTeam2.setText("");
    } else {
      hodlerMatch.tvScoreTeam2.setText(team2.getScore());
      hodlerMatch.tvOverTeam2.setText(String.valueOf(team2.getOvers()));
    }
    if (team2.getWinner()) {
      hodlerMatch.imageView_upperteam2.setVisibility(View.VISIBLE);
    } else {
      hodlerMatch.imageView_upperteam2.setVisibility(View.GONE);
    }

    if (Constant.CLOSED.equalsIgnoreCase(contest.getMatchStatus())){
      hodlerMatch.matchStatus.setBackgroundResource(R.drawable.background_statustextview_green);

  }else {
      hodlerMatch.matchStatus.setBackgroundResource(R.drawable.background_statustextview);

  }
    hodlerMatch.matchStatus.setText(" • "+contest.getMatchStatus()+" • ");
    hodlerMatch.rvContest.setAdapter(new ContestAdapter(mcontext,callback,viewPool));
    ((ContestAdapter)hodlerMatch.rvContest.getAdapter()).setCotestList(contest.getContestList());

}
    @Override
    public int getItemCount() {

      if(cotestList!=null)
        return cotestList.size();

      else
       return 0;
    }

 public void addItem(ArrayList<Object> arrayList){
      int currentSize = cotestList.size();
   int itemcount = arrayList.size();
   cotestList.addAll(arrayList);
   notifyItemRangeInserted(currentSize, itemcount);
 }

  @Override
  public int getItemViewType(int position) {
    if(cotestList.get(position) instanceof  String){
      return 1;
    }else{
      return 2;
    }
  }
}
