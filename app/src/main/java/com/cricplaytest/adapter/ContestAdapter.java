package com.cricplaytest.adapter;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.cricplaytest.R;
import com.cricplaytest.models.ContestList;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by ritesh5.singh on 29/10/18.
 */

public class ContestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private MatchAdapter.Callback callback;
  private Context mcontext;
  private ArrayList<ContestList> cotestList;
  private RecyclerView.RecycledViewPool mViewPool;
  public interface Callback {
    void onSelectGoTOContest(ContestList contest);
  }

  public ContestAdapter(Context context, MatchAdapter.Callback callback,RecyclerView.RecycledViewPool viewPool) {
    this.mcontext = context;
    this.callback = callback;
    cotestList = new ArrayList<>();
    mViewPool= viewPool;
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    RecyclerView.ViewHolder viewHolder = null;
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());
    switch (viewType) {
      case 1:
        viewHolder = new ViewHolderPublic(inflater.inflate(R.layout.public_contest_row, parent, false),mViewPool);
        break;
      case 2:
        viewHolder = new ViewHolderPrivate(inflater.inflate(R.layout.private_contest_row, parent, false),mViewPool);
        break;
    }

    return viewHolder;
  }

   class ViewHolderPublic extends RecyclerView.ViewHolder {
    TextView tvprizeAmount;
    TextView totalCoins;
    TextView totalWinner;
    TextView totalPlaying;
    RecyclerView rvRank;


    public ViewHolderPublic(View itemView, RecyclerView.RecycledViewPool viewPool) {
      super(itemView);

       tvprizeAmount = itemView.findViewById(R.id.tvprizeAmount);
       totalCoins = itemView.findViewById(R.id.totalCoins);
       totalWinner = itemView.findViewById(R.id.totalWinner);
       totalPlaying = itemView.findViewById(R.id.totalPlaying);
       rvRank = itemView.findViewById(R.id.rvRank);
      rvRank.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false));
      rvRank.addItemDecoration(new DividerItemDecoration(mcontext, DividerItemDecoration.VERTICAL));
      rvRank.setRecycledViewPool(viewPool);
      rvRank.setAdapter(new TeamRankAdapter(mcontext));
    }
  }

   class ViewHolderPrivate extends RecyclerView.ViewHolder {
    ImageView ivfullBg;
    TextView tvContestName;
    TextView tvprizeAmount;
    TextView totalCoins;
    TextView totalWinner;
    RecyclerView rvRankPrivate;

    public ViewHolderPrivate(View itemView,RecyclerView.RecycledViewPool viewPool) {
      super(itemView);
       ivfullBg = itemView.findViewById(R.id.ivfullBg);
       tvContestName = itemView.findViewById(R.id.tvContestName);
       tvprizeAmount = itemView.findViewById(R.id.tvprizeAmount);
       totalCoins = itemView.findViewById(R.id.totalCoins);
       totalWinner = itemView.findViewById(R.id.totalWinner);
      rvRankPrivate = itemView.findViewById(R.id.rvRankPrivate);
      rvRankPrivate.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL, false));
      rvRankPrivate.addItemDecoration(new DividerItemDecoration(mcontext, DividerItemDecoration.VERTICAL));
      rvRankPrivate.setRecycledViewPool(viewPool);
      rvRankPrivate.setAdapter(new TeamRankAdapter(mcontext));
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    switch (holder.getItemViewType()) {
      case 1:
        ViewHolderPublic viewHolderPublic = (ViewHolderPublic) holder;
        bindViewPublic(viewHolderPublic, position);
        break;
      case 2:
        ViewHolderPrivate viewHolderContest = (ViewHolderPrivate) holder;
        bindViewContest(viewHolderContest, position);
        break;
    }
  }

  private void bindViewPublic(ViewHolderPublic viewHolderPublic, int pPosition) {
    ContestList contest= cotestList.get(pPosition);
    viewHolderPublic.tvprizeAmount.setText(String.valueOf(contest.getTotalprize()));
    viewHolderPublic.totalCoins.setText(String.valueOf(contest.getTotalCoin()));
    viewHolderPublic.totalWinner.setText(String.valueOf(contest.getTotalWinning()));
    viewHolderPublic.totalPlaying.setText(String.valueOf(contest.getPlaying()));
    ((TeamRankAdapter)viewHolderPublic.rvRank.getAdapter()).addItem(contest.getUserTeams());

  }

  private void bindViewContest(ViewHolderPrivate hodlerMatch, int pPosition) {

    ContestList contest = (ContestList) cotestList.get(pPosition);
    if(!TextUtils.isEmpty(contest.getCoverImage())){
      Picasso.get()
          .load(contest.getCoverImage())
          .fit()
          .placeholder(R.drawable.mumbai)
          .error(R.drawable.mumbai)
          .into(hodlerMatch.ivfullBg);
    }

    hodlerMatch.tvContestName.setText(contest.getCreatorName()+mcontext.getString(R.string.pvt_message));
    hodlerMatch.tvprizeAmount.setText(String.valueOf(contest.getMaxTeams()));
    hodlerMatch.totalCoins.setText(String.valueOf(contest.getMaxTeamsPerUser()));
    hodlerMatch.totalWinner.setText(String.valueOf(contest.getPlaying()));
   ((TeamRankAdapter)hodlerMatch.rvRankPrivate.getAdapter()).addItem(contest.getUserTeams());
  }
  @Override
  public int getItemCount() {

    if(cotestList!=null)
      return cotestList.size();

    else
      return 0;
  }

public void setCotestList(ArrayList<ContestList> pContest){
  cotestList.clear();
  cotestList.addAll(pContest);
  notifyDataSetChanged();
}
  @Override
  public int getItemViewType(int position) {
    if(cotestList.get(position).getType().equals("PUBLIC")){
      return 1;
    }else{
      return 2;
    }
  }
}
