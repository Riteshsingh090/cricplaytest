package com.cricplaytest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.cricplaytest.R;
import com.cricplaytest.models.UserTeam;
import java.util.ArrayList;

/**
 * Created by ritesh5.singh on 29/10/18.
 */

public class TeamRankAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Context mcontext;
  private ArrayList<UserTeam> rankList;


  public TeamRankAdapter(Context context) {
    this.mcontext = context;

    rankList = new ArrayList<>();
  }

  @Override
  public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    RecyclerView.ViewHolder viewHolder = null;
    LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        viewHolder = new ViewHolderUserTeam(inflater.inflate(R.layout.contest_rank_row, parent, false));


    return viewHolder;
  }


  class ViewHolderUserTeam extends RecyclerView.ViewHolder {

    TextView rankTEXT;
    TextView teamTEXT;
    TextView pointsTEXT;


    public ViewHolderUserTeam(View itemView) {
      super(itemView);

      rankTEXT = itemView.findViewById(R.id.rankTEXT);
      teamTEXT = itemView.findViewById(R.id.teamTEXT);
      pointsTEXT = itemView.findViewById(R.id.pointsTEXT);
    }
  }

  @Override
  public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


    ViewHolderUserTeam viewHolderuserTeam = (ViewHolderUserTeam) holder;
    UserTeam userTeam =  rankList.get(position);
    viewHolderuserTeam.rankTEXT.setText(String.valueOf(userTeam.getRank()));
    viewHolderuserTeam.teamTEXT.setText(userTeam.getName());
    viewHolderuserTeam.pointsTEXT.setText(String.valueOf(userTeam.getPoints()));

  }




  @Override
  public int getItemCount() {

    if(rankList!=null)
      return rankList.size();

    else
      return 0;
  }

  public void addItem(ArrayList<UserTeam> arrayList){
    rankList.addAll(arrayList);
    notifyDataSetChanged();
  }


}
