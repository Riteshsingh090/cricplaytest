
package com.cricplaytest.models;

import com.cricplaytest.constant.Constant;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContestList {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("playing")
    @Expose
    private Integer playing;
    @SerializedName("maxTeams")
    @Expose
    private Integer maxTeams;
    @SerializedName("maxTeamsPerUser")
    @Expose
    private Integer maxTeamsPerUser;
    @SerializedName("creatorName")
    @Expose
    private String creatorName;
    @SerializedName("coverImage")
    @Expose
    private String coverImage;
    @SerializedName("winnerPrizes")
    @Expose
    private List<WinnerPrize> winnerPrizes = null;
    @SerializedName("userTeams")
    @Expose
    private ArrayList<UserTeam> userTeams = null;

    private long totalCoin;
    private long totalWinning;
    private long totalprize;

    public Long getTotalprize() {
        return totalprize;
    }

    public void setTotalprize(Long totalprize) {
        this.totalprize = totalprize;
    }

    public Long getTotalCoin() {
        return totalCoin;
    }

    public void setTotalCoin(Long totalCoin) {
        this.totalCoin = totalCoin;
    }

    public Long getTotalWinning() {
        return totalWinning;
    }

    public void setTotalWinning(Long totalWinning) {
        this.totalWinning = totalWinning;
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getPlaying() {
        return playing;
    }

    public void setPlaying(Integer playing) {
        this.playing = playing;
    }

    public Integer getMaxTeams() {
        return maxTeams;
    }

    public void setMaxTeams(Integer maxTeams) {
        this.maxTeams = maxTeams;
    }

    public Integer getMaxTeamsPerUser() {
        return maxTeamsPerUser;
    }

    public void setMaxTeamsPerUser(Integer maxTeamsPerUser) {
        this.maxTeamsPerUser = maxTeamsPerUser;
    }

    public String getCreatorName() {
        return creatorName;
    }

    public void setCreatorName(String creatorName) {
        this.creatorName = creatorName;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public List<WinnerPrize> getWinnerPrizes() {
        return winnerPrizes;
    }

    public void setWinnerPrizes(List<WinnerPrize> winnerPrizes) {
        this.winnerPrizes = winnerPrizes;
    }

    public ArrayList<UserTeam> getUserTeams() {
        return userTeams;
    }

    public void setUserTeams(ArrayList<UserTeam> userTeams) {
        this.userTeams = userTeams;
    }

    public void setUserContestValues() {
        if(winnerPrizes!=null && !winnerPrizes.isEmpty()){
            for(WinnerPrize prize : winnerPrizes){
                if(Constant.WINNING.equals(prize.getCurrency())){
                    totalprize+=prize.getTotalPrize();
                }else{
                    totalCoin+=prize.getTotalPrize();
                }
                totalWinning+=prize.getTotalWinner();
            }
        }

    }

}
