
package com.cricplaytest.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WinnerPrize {

    @SerializedName("totalWinner")
    @Expose
    private Integer totalWinner;
    @SerializedName("totalPrize")
    @Expose
    private Integer totalPrize;
    @SerializedName("currency")
    @Expose
    private String currency;

    public Integer getTotalWinner() {
        return totalWinner;
    }

    public void setTotalWinner(Integer totalWinner) {
        this.totalWinner = totalWinner;
    }

    public Integer getTotalPrize() {
        return totalPrize;
    }

    public void setTotalPrize(Integer totalPrize) {
        this.totalPrize = totalPrize;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
