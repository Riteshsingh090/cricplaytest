
package com.cricplaytest.models;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyContest {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("matchStatus")
    @Expose
    private String matchStatus;
    @SerializedName("startTime")
    @Expose
    private Long startTime;
    @SerializedName("team1")
    @Expose
    private Team1 team1;
    @SerializedName("team2")
    @Expose
    private Team2 team2;
    @SerializedName("contestList")
    @Expose
    private ArrayList<ContestList> contestList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMatchStatus() {
        return matchStatus;
    }

    public void setMatchStatus(String matchStatus) {
        this.matchStatus = matchStatus;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Team1 getTeam1() {
        return team1;
    }

    public void setTeam1(Team1 team1) {
        this.team1 = team1;
    }

    public Team2 getTeam2() {
        return team2;
    }

    public void setTeam2(Team2 team2) {
        this.team2 = team2;
    }

    public ArrayList<ContestList> getContestList() {
        return contestList;
    }

    public void setContestList(ArrayList<ContestList> contestList) {
        this.contestList = contestList;
    }

}
