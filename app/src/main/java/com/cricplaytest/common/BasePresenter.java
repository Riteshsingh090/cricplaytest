package com.cricplaytest.common;

import android.support.annotation.NonNull;

import com.cricplaytest.data.DataManager;
import io.reactivex.disposables.CompositeDisposable;



public abstract class BasePresenter<V extends BaseContract.View>
    implements BaseContract.Presenter<V> {

  private static final String TAG = "BasePresenter";

  private CompositeDisposable compositeDisposable;

  private V view;

  public BasePresenter(
     ) {
    this.compositeDisposable = new CompositeDisposable();
  }

  @Override
  public void onCreate(V view) {
    this.view = view;
    if (compositeDisposable.isDisposed()) {
      compositeDisposable = new CompositeDisposable();
    }
  }

  @Override
  public void onDestroy() {
    if (!compositeDisposable.isDisposed()) {
      compositeDisposable.dispose();
    }
    view = null;
  }

  public boolean isViewAttached() {
    return view != null;
  }

  public V getView() {
    return view;
  }


  protected CompositeDisposable getCompositeDisposable() {
    return compositeDisposable;
  }


}
