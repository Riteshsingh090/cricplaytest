package com.cricplaytest.common;




public interface BaseContract {

  /**
   * Base class for all presenters in mvp.
   */
  interface Presenter<V extends View> {

    void onCreate(V view);

    void onDestroy();
  }

  /**
   * Base class for all views in mvp.
   */
  interface View {
    void showLoading();

    void hideLoading();

    void showMessage(int message);

  }
}
