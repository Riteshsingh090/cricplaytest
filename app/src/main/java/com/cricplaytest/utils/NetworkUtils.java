package com.cricplaytest.utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by ritesh5.singh on 28/10/18.
 */

public class NetworkUtils {

  public static boolean isNetWorkAvailable(Context context) {
    ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

      if (connMgr == null) {
        return false;
      } else if (connMgr.getActiveNetworkInfo() != null && connMgr.getActiveNetworkInfo().isAvailable() && connMgr.getActiveNetworkInfo().isConnected()) {
        return true;
      } else {
        return false;
      }

  }
}
