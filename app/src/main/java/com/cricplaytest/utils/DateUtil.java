package com.cricplaytest.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ritesh5.singh on 28/10/18.
 */

public class DateUtil {

  public static String getDateFromTime(long time){
    Date date=new Date(time);
    SimpleDateFormat df2 = new SimpleDateFormat("dd MMM yyyy");
    return df2.format(date);
  }
}
