package com.cricplaytest.utils;

import com.cricplaytest.constant.Constant;
import com.cricplaytest.models.ContestList;
import com.cricplaytest.models.MyContest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ritesh5.singh on 31/10/18.
 */

public class AppUtils {

  public static List<Object> getObjectList(List<MyContest> myContests){
    ArrayList<Object> tempList=null;
    if(myContests!=null && myContests.size()>0){
      tempList= new ArrayList<>() ;
      String date = null;
      String lastSavedDate = null;
      for(MyContest myContest : myContests){
        date=DateUtil.getDateFromTime(myContest.getStartTime());
        if(!date.equals(lastSavedDate)){
          tempList.add(date);
          lastSavedDate= date;
        }
        // Pre Calculation of values
          if(myContest.getContestList()!=null && myContest.getContestList().size()>0){
            for(ContestList contest: myContest.getContestList()) {
              if(contest.getType().equals(Constant.PUBLIC)){
                contest.setUserContestValues();
              }
            }
          }
        tempList.add(myContest);
      }
    }
    return tempList;
  }


}
