package com.cricplaytest.data;

import com.cricplaytest.models.MyContest;
import com.cricplaytest.network.RetrofitUtils;
import com.cricplaytest.network.URLConstant;
import io.reactivex.Single;
import java.util.List;

/**
 * Created by ritesh5.singh on 30/10/18.
 */

public class DataManager {

  private static DataManager instance;

  public static DataManager getInstance() {
    if (instance == null) {
      synchronized (DataManager.class) {
        instance = new DataManager();
      }
    }
    return instance;
  }

  public Single<List<MyContest>> getMyContest(String token,int currentPage, int countPerPage){
     /*
      * It will be useful if we have to support both online and offline support
      * from here we will return Single and based on data is present locally or not we will get data from API
      */
    return RetrofitUtils.getInstance().getService().getMyContest(token,currentPage,countPerPage);
  }

}
