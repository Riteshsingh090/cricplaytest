package com.cricplaytest.presenter;

import android.content.Context;
import com.cricplaytest.common.BaseContract;
import com.cricplaytest.models.ContestList;
import java.util.ArrayList;
import java.util.List;

public interface ContestContract {

  interface Presenter extends BaseContract.Presenter<View> {
    void loadContestList(Context context);

    boolean isLoading();

    boolean isLastPage();

  }

  interface View extends BaseContract.View {

    void goTOContest(ContestList contest);

    void addContestList(ArrayList<Object> response);

    void showEmptyView();
  }
}
