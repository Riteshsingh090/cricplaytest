package com.cricplaytest.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import com.cricplaytest.R;
import com.cricplaytest.common.BasePresenter;
import com.cricplaytest.constant.Constant;
import com.cricplaytest.data.DataManager;
import com.cricplaytest.models.MyContest;
import com.cricplaytest.network.URLConstant;
import com.cricplaytest.utils.AppUtils;
import com.cricplaytest.utils.NetworkUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;

public class ContestPresenter extends BasePresenter<ContestContract.View>
    implements ContestContract.Presenter {

  private boolean isLoading = false;
  private boolean isLastPage = false;
  private int currentPage = 1;

  public ContestPresenter() {
    super();

  }

  @Override
  public void onCreate(ContestContract.View view) {
    super.onCreate(view);
  }

  @Override
  public void loadContestList(Context pContext) {

    if(NetworkUtils.isNetWorkAvailable(pContext)) {
      this.isLastPage = false;

      getCompositeDisposable().add(DataManager.getInstance()
          .getMyContest(URLConstant.BEARER_TOKEN, currentPage, Constant.CONTEST_PER_PAGE)
          .map(new Function<List<MyContest>, List<Object>>() {
            @Override
            public List<Object> apply(List<MyContest> myContests) throws Exception {
              return AppUtils.getObjectList(myContests);
            }
          })
          .subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe(this::handleResponse, Throwable::printStackTrace));
    }else{
      getView().showMessage(R.string.no_network);
    }
  }

  @Override
  public boolean isLoading() {
    return isLoading;
  }

  @Override
  public boolean isLastPage() {
    return isLastPage;
  }

  public void handleResponse(List<Object> list) {
    if (list != null && list.size()>0) {

      setCurrentPage();
      getView().addContestList((ArrayList<Object>)list);
    }else{
      if(currentPage==0){
        getView().showEmptyView();
      }
      isLastPage = true;
    }
    isLoading = false;
        //.subscribe(this::onCartDetails, Throwable::printStackTrace));
  }

  public void handleError(Throwable error) {
    getView().showMessage(R.string.serverError);
    error.printStackTrace();
    //.subscribe(this::onCartDetails, Throwable::printStackTrace));
  }

  public void setCurrentPage() {
    this.currentPage++;
  }
}
