package com.cricplaytest.network;

import android.content.Context;
import android.text.TextUtils;
import com.cricplaytest.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import java.io.IOException;
import java.lang.annotation.Annotation;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by ritesh singh on 28/10/18.
 */

public class RetrofitUtils {
    private static final String TAG = "RetrofitUtils";
    private static RetrofitUtils sInstance;
    public static final long LOW_PRIORITY_TIMEOUT = 30 * 1000; // 30 Seconds
    public static final long MEDIUM_PRIORITY_TIMEOUT = 60 * 1000; // 60 Seconds
    public static final long HIGH_PRIORITY_TIMEOUT = 120 * 1000; // 120 Seconds
   // Default value
    private long mRequestTimeOut = LOW_PRIORITY_TIMEOUT;
    private ResponseType mResponseType = ResponseType.RESPONSE_TYPE_GSON;
    private Gson gson;

    public enum ResponseType {
        RESPONSE_TYPE_GSON,
        RESPONSE_TYPE_STRING,
    }

    private RetrofitUtils() {

    }

    public static RetrofitUtils getInstance() {
        if (sInstance == null) {
            sInstance = new RetrofitUtils();
        }
        return sInstance;
    }

    public void setRequestTimeOut(long mConnectTimeOut) {
        this.mRequestTimeOut = mConnectTimeOut;
    }

    public void setResponseType(ResponseType responseType) {
        mResponseType = responseType;
    }

    public String getBaseURL() {

        return URLConstant.BASE_URL;
    }

    public APIInterface getService() {

        return buildRetrofitAdapter(getBaseURL()).create(APIInterface.class);
    }


  private Retrofit buildRetrofitAdapter(String baseUrl) {
        if ( TextUtils.isEmpty(baseUrl)) {
            return null;
        }
        if (mResponseType == ResponseType.RESPONSE_TYPE_GSON) {
            GsonBuilder bulder= new GsonBuilder() ;
            bulder.serializeNulls();
            bulder.setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
            gson = bulder.create();
        }

      HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
      if(BuildConfig.DEBUG)
      interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
      OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                 .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


}