package com.cricplaytest.network;

import com.cricplaytest.models.MyContest;
import io.reactivex.Single;
import java.util.List;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;


/**
 * Created by ritesh singh on 28/10/18.
 */

public interface APIInterface {

   @GET("contest")
    Single<List<MyContest>> getMyContest(@Header("Authorization") String authorization,@Query("page") Integer page, @Query("size") Integer size);
}