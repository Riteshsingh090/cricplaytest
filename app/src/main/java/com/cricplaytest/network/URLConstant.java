package com.cricplaytest.network;

/**
 * Created by ritesh singh on 28/10/18.
 */

public class URLConstant {
   // http://stgapi.cricplay.com/cric/user/test/27243041-46a3-4588-9445-9b71d24da6b9/contest?page=<page_number>&size=<page_size>

    public static final String BASE_URL="http://stgapi.cricplay.com/cric/user/test/27243041-46a3-4588-9445-9b71d24da6b9/";

    public static final String BEARER_TOKEN="Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI5OTcxNDcxODExIn0.5J9CO474CE_kR2f_7HqfcJujHSw7tA0T7oR1wZN0_TMHAFNpNbjoujTFfZe-B-96RkEd9SlZrIuRimTSuCPFow";
}
