package com.cricplaytest.constant;

/**
 * Created by ritesh5.singh on 28/10/18.
 */

public class Constant {

  public static final  String TOBAT= "TOBAT";
  public static final  String CLOSED= "close";
  public static final int TOTAL_PAGES = Integer.MAX_VALUE;
  public  static final int CONTEST_PER_PAGE = 5;
  public  static final String WINNING = "WINNING";
  public  static final String PUBLIC = "PUBLIC";


}
